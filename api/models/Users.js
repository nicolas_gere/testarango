/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	schema: true,
	tableName: 'users',
	types: {
		uniqueEmail: function(value) {
			return uniqueEmail ;         
		}
	},
	attributes: {
		id: {
			type: 'string',
			primaryKey: true,
			columnName: '_key'
		},
		email: {
			type: 'string',
			email: true,
			//unique: true, *Pas implemente avec arango, a faire*
			required: true,
			uniqueEmail:true
		},
		firstname:{
			type: 'string',
			required: true,
			minLength: 2,
			maxLength: 50,

		},
		lastname:{
			type: 'string',
			required: true,
			minLength: 2,
			maxLength: 50
		},
		age:{
			type: 'integer',
			required: true,
			min:18,
			max:150
		}
	},
	beforeValidate:function(values,cb){
		Users.find({email: values.email}).exec(function (err, r) {
			if(values.id){
				var filteredR = r.filter(function(item){ //TODO implement le 'not' dans le connecteur sails-arango pour eviter cela, 
					return item.id != values.id       //OU encore mieu, le type unique
				});
				uniqueEmail = !err && filteredR.length==0;
			}else{
				uniqueEmail = !err && r.length==0;
			}
			cb();
		});
	},
	beforeCreate:function(values,cb){
		delete values.id; //Permet de ne pas autoriser de créer un user avec un id envoyer par le client.
		cb();
	}
};


