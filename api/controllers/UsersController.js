/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {
 	create: function(req, res) {
 		Users.create(req.body).exec(function(err, r){
 			if(err){
 				res.status(err.status);
 				return res.json(err);
 			}else{
 				return res.json(r);
 			}
 		});
 	},
 	delete: function(req, res) {
 		Users.destroy({_key:req.params.user_id}).exec(function(err, r){
 			if(err){
 				res.status(err.status);
 				return res.json(err);
 			}else{
 				return res.json(r);
 			}
 		});
 	},
 	update:function(req,res){
 		var data = req.body;
 		data.id = req.params.user_id;
 		Users.update({_key:req.params.user_id},data).exec(function(err,r){
 			if(err){
 				res.status(err.status);
 				return res.json(err);
 			}else{
 				return res.json(r);
 			}
 		});
 	},
 	find: function(req, res) {
 		Users.find({_key:req.params.user_id}).exec(function(err, r){
 			if(err){
 				res.status(err.status);
 				return res.json(err);
 			}else{
 				return res.json(r);
 			}
 		});
 	},
 	all: function(req, res) {
 		Users.find().exec(function(err, r){
 			if(err){
 				res.status(err.status);
 				return res.json(err);
 			}else{
 				return res.json(r);
 			}
 		});
 	},
 };

